# Sequenza: Copy Number Estimation from Tumor Genome Sequencing Data

[![CRAN_Status_Badge](http://www.r-pkg.org/badges/version/sequenza)](https://cran.r-project.org/package=sequenza)
[![CRAN_Downloads_Badge](http://cranlogs.r-pkg.org/badges/sequenza)](https://cran.r-project.org/package=sequenza)


Sequenza is a tool to analyze genomic sequencing data from paired normal-tumor samples, including cellularity and ploidy estimation; mutation and copy number (allele-specific and total copy number) detection, quantification and visualization.

## Installation

```R
# install.packages("devtools")
library(devtools)
install_bitbucket("patidarr/sequenza")
```



This is a fork from ffavero; which at some point should be merged to the main